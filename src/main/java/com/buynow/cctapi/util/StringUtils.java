package com.buynow.cctapi.util;

import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by jinyunni on 2017-06-05.
 */

public class StringUtils {

    /**
     * 获取新会员ID
     *
     * @return
     */
    public static String GetNewMemberID() {
        return UUID.randomUUID().toString();
    }

    /***
     * 手机号码最后一位
     *
     * @param strMobile
     * @return
     */
    public static String GetMobileLastNumber(String strMobile) {
        return strMobile.substring(strMobile.length() - 1);

    }

    /**
     * 是否大陆手机
     *
     * @param mobiles
     * @return
     */
    public static boolean isMobileNumber(String mobiles) {
        return Pattern
                .compile("^((13[0-9])|(15[^4,\\D])|(18[^1^4,\\D]))\\d{8}")
                .matcher(mobiles).matches();
    }


    /**
     * 新生成会员编号前-位
     *
     * @param memberID
     * @return
     */
    public static String GetMemberIDTopOne(String memberID) {
        return memberID.substring(0, 1);

    }


    /**
     * 新生成会员编号前二位
     *
     * @param memberID
     * @return
     */
    public static String GetMemberIDTopTwo(String memberID) {
        return memberID.substring(0, 2);

    }

    /**
     * 根据手机号码,获取对应的表名
     *
     * @param strMobile
     * @return
     */
    public static String GetMobileTableName(String strMobile) {


        return String.format("CCT_DBA_CCT_MEMBERBYMOBILE_%s", GetMobileLastNumber(strMobile));

    }

    /**
     * 根据wx号码,获取对应的表名
     *
     * @param wxD
     * @return
     */
    public static String GetWxTableName(String wxD) {


        return String.format("CCT_DBA_CCT_MEMBERBYWX_%s", GetMemberIDTopOne(wxD));
    }


    /**
     * 根据member号码,获取对应的表名
     *
     * @param memberID
     * @return
     */
    public String GetMemberTableName(String memberID) {


        return String.format("CCT_DBA_CCT_MEMBER_%s", GetMemberIDTopTwo(memberID));
    }
}