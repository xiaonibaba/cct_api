package com.buynow.cctapi.constant;

/**
 * @author xiaonibaba
 */
public class ResourceNameConstant {

    public static final String Member = "Member";

    /**
     * Prevent instantiation
     */
    private ResourceNameConstant() {
    }

}
