package com.buynow.cctapi.constant;

/**
 * @author xiaonibaba
 */
public class ErrorCode {

    public static final int SERVER_INTERNAL_ERROR = 500;
    public static final int PARAMETER_MISSING_ERROR = 504;
    public static final int PARAMETER_ILLEGAL_ERROR = 401;
    public static final int RESOURCE_NOT_FOUND_ERROR = 404;
    public static final int UNAUTHORIZED_ERROR =403;

    /**
     * Prevent instantiation
     */
    private ErrorCode() {
    }

}
