package com.buynow.cctapi.web;

import io.swagger.annotations.ApiImplicitParam;

/**
 * Created by jinyunni on 2017-06-05.
 */
public @interface Token {
    ApiImplicitParam[] value() default {@ApiImplicitParam(name = "Token", value = "access_token", required = true, dataType = "string", paramType = "header")};

    //ApiImplicitParams value() default @ApiImplicitParams({@ApiImplicitParam(name = "Token", value = "access_token", required = true, dataType = "string", paramType = "header")});
}
