package com.buynow.cctapi.web;

import com.buynow.cctapi.dao.MemberDao;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jinyunni on 2017-06-05.
 */
@RestController
@RequestMapping("api/v1/Member")
public class MemberController {

    @PostMapping
    @ApiImplicitParams({@ApiImplicitParam(name = "Token", value = "access_token", required = true, dataType = "string", paramType = "header")})
    public ResponseEntity<?> PostMemberByMobile(@RequestBody String mobile) {

        String uuid = MemberDao.CreateMemberByMobile(mobile);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(uuid);
    }
}
