package com.buynow.cctapi.web;

import com.buynow.cctapi.constant.PaginatedResult;
import com.buynow.cctapi.util.db.DbBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017-06-04.
 */
@RestController
@RequestMapping("api/v1/hello")
public class HelloController {

    @GetMapping
     public String index(){

       int count = DbBuilder.getCount("select count(*) from cct_dba_cct_memberbymobile_0");

       return "hello world" + count;
    }

    @GetMapping("/{id}")
    //@ApiImplicitParams({@ApiImplicitParam(name = "Token", value = "access_token", required = true, dataType = "string", paramType = "header")})
    public    Map<String, Object> index1(@PathVariable String id){

        Map<String, Object> obj = DbBuilder.getFirstRowMap("select * from cct_dba_cct_memberbymobile_0");

        return obj;
    }

    @GetMapping("/h2")
    public List<Map<String, Object>> index2(){

        Connection conn = DbBuilder.getConnection("spring.datasource.url","spring.datasource.username",
                "spring.datasource.password","spring.datasource.driver-class-name");

        List<Map<String, Object>> users = DbBuilder.getListMap(conn,"select * from cct_dba_cct_memberbymobile_0");

        DbBuilder.close(conn);

        return users;
    }

    @GetMapping("/h3")
    public ResponseEntity<?> index3(){

        return ResponseEntity
                .ok(new PaginatedResult()
                        .setData(DbBuilder.getListMap("select * from cct_dba_cct_memberbymobile_0"))
                        .setCurrentPage(1)
                        .setTotalPage(2));
    }

}
